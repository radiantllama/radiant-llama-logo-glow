(function(){
  // prepare base perf object
  if (typeof window.performance === 'undefined') {
      window.performance = {};
  }
  if (!window.performance.now){
    var nowOffset = Date.now();
    if (performance.timing && performance.timing.navigationStart){
      nowOffset = performance.timing.navigationStart;
    }
    window.performance.now = function now(){
      return Date.now() - nowOffset;
    };
  }
})();

Vector = (function() {
	var Vector = function (x,y) {
		this.x = x;
		this.y = y;
	};
	// prototypes
	Vector.prototype.logCoords = function () {
		console.log(this.x + " " + this.y);
	};
	Vector.prototype.add = function (vector) {
		//console.log(llamaGlow.moveOffset);
		var offset = llamaGlow.getFramesElapsed() || 1;
		this.x += vector.x * offset;
		this.y += vector.y * offset;
	};
	Vector.prototype.getMagnitude = function () {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	};
	Vector.prototype.getAngle = function () {
		return Math.atan2(this.y,this.x);
	};
	Vector.prototype.fromAngle = function (angle,magnitude) {
		return new Vector(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	};
	
	return Vector;

})();

Particle = (function() {
	var Particle = function (point, velocity, acceleration) {
		this.position = point || new Vector(0, 0);
		this.velocity = velocity || new Vector(0, 0);
		this.acceleration = acceleration || new Vector(0, 0);
	};
	Particle.prototype.move = function () {
		this.velocity.add(this.acceleration);
		this.position.add(this.velocity);
	};

	return Particle;
})();

Emitter = (function () {
	var Emitter = function (point, velocity) {
		this.position = point; // Vector
		this.velocity = velocity; // Vector
	};
	Emitter.prototype.emitParticle = function() {
		// The angle of emitter's velocity
		var angle = this.velocity.getAngle();
		// The magnitude of the emitter's velocity
		var magnitude = this.velocity.getMagnitude();
		// The emitter's position  
		var position = new Vector(this.position.x, this.position.y);
		// New velocity based off of the calculated angle and magnitude
		var velocity = position.fromAngle(angle,magnitude);  //new Vector( magnitude * Math.cos(angle), magnitude * Math.sin(angle) );// .fromAngle(angle, magnitude);
		// return our new Particle!
		var newParticle = new Particle(position,velocity);
		
		return newParticle;
	};

	return Emitter;
})();

llamaGlow = (function () {
	var current,
		delta,
		fps = 45,
		last = 0,
		framesElapsed,
		newAtFramesElapsed = 15,
		framesSinceNew = 0,
		runAnimation = true,
		magnitude = 1.5,
		maxParticles,
		canvas = document.querySelector('canvas'),
		ctx = canvas.getContext('2d'),
		particleSize = 6,
		particles = [],
		position, di, ei, mi,
		midX, midY,
		emitters = Array(),
		emitFrom, emitPoint,
		maxEmissions,
		alpha, pcolor, rays;

	var setup = function() {
		var piMultiplier,
			addToMult = 0,
			rays = 16,
			maxModemits;
			

		canvas.width = 750;
		canvas.height = 750;
		midX = canvas.width / 2;
		midY = canvas.height / 2;
		emitPoint = new Vector(midX-(particleSize/2), midY-(particleSize/2));
		alpha = 1;

		for ( var i = 0; i < rays; i++ ) {
			piMultiplier = (i/8);
			emitters.push( new Emitter(emitPoint, emitPoint.fromAngle(Math.PI * piMultiplier, magnitude) ) );
		}

		// The max number of particles that can fit within the canvas box without bleeding out
		// I have no idea why this equation works or how I got to it......
		maxParticles = Math.floor( (( rays * ( ( (canvas.width/2)/particleSize )/(4)/magnitude ) ))*(particleSize/4) );
		// if maxParticles is not a multiple of the number of emitters/rays, make it one!
		maxModemits = maxParticles % emitters.length;
		maxParticles = (maxModemits!==0) ? (emitters.length-maxModemits)+maxParticles : maxParticles;
		// max number of rings
		maxEmissions = maxParticles/emitters.length;

		// draw the max allowed particles before moving, so glow starts strong before fading
		for ( var z = 0; z < maxParticles; z++ ) {
			particles.push( emitters[z%emitters.length].emitParticle() );
		}
	};

	var clear = function() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	};
	var firstTime = true;
	var update = function() {
		// move existing particles
		for (mi = 0; mi < particles.length; mi++) {
			particles[mi].move();
		}
		// emit new particles after reaching desired frame rate
		if ( framesSinceNew >= newAtFramesElapsed ) {
			framesSinceNew = 0;
			for ( ei = 0; ei < emitters.length; ei++ ) {
				particles.push( emitters[ei].emitParticle() );
			}
		}
	};

	
	var draw = function() {
		for (di = 0; di < particles.length; di++) {
			if ( di > maxParticles ) {
				particles.splice(0,emitters.length);
				continue;
			}
			pcolor = (di%2 === 0) ? "#F16739" :  "#FFCC05";
			ctx.fillStyle = pcolor;
			if ( di%(emitters.length)===0 ) {
				if ( di===0 ) {
					alpha = 1/maxEmissions;
				} else {
					alpha += (1/maxEmissions);
				}
				ctx.globalAlpha = alpha.toFixed(3);
				//console.log(alpha);
			}
			position = particles[di].position;
			ctx.fillRect(position.x, position.y, particleSize, particleSize);
		}
	};

	return {
		getFramesElapsed: function() {
			return framesElapsed;
		},
		init: function() {
			setup();
			setTimeout(function() {
			//	runAnimation = false;
			}, 1000);
		},
		loop: function() {
			current = performance.now();
			delta = performance.now() - last;
			// A movement offset to ensure frame rate fluctuations do not affect movement distance
			// Based on the ms delta between this frame and last frame, rounds to an integer (faster than Math.round)
			framesElapsed = (delta/(1000/fps));
			framesSinceNew += framesElapsed;
			//console.log(framesElapsed + );
			last = current;

			if (runAnimation) {
				clear();
				update();
				draw();
				window.requestAnimationFrame( llamaGlow.loop );
			}

		},
		createObject: function (o) {
			function F(){}
			F.prototype = o;
			return new F();
		}
	};

})();

llamaGlow.init();
llamaGlow.loop();